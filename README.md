# Semantic Release Impact

Determines the impact of a branch on a potential semantic release relative to
another branch.
